package com.dinfo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by Carlos on 2017/12/28.
 */
@Controller
@RequestMapping("/mts")
public class MapTileController {

    @RequestMapping(value="/getTile", method= RequestMethod.GET)
    public String getTile(HttpServletRequest request, HttpServletResponse response) {
        System.out.println("start");
    	String x = request.getParameter("x");
        String y = request.getParameter("y");
        String z = request.getParameter("z");
        String fileName = y + ".png";
        return "forward:/mts/normal/"+z+"/"+x+"/"+fileName;
    }

    @RequestMapping(value="/getHybridMapTile", method= RequestMethod.GET)
    public String getHybridMapTile(HttpServletRequest request, HttpServletResponse response) {
    	System.out.println("start get map");
    	String x = request.getParameter("x");
        String y = request.getParameter("y");
        String z = request.getParameter("z");
        String fileName = y + ".png";
        return "forward:/mts/Hybrid/MapTiles/"+z+"/"+x+"/"+fileName;
    }

    @RequestMapping(value="/getHybridRoadTile", method= RequestMethod.GET)
    public String getHybridRoadTile(HttpServletRequest request, HttpServletResponse response) {
    	System.out.println("start get road");
    	String x = request.getParameter("x");
        String y = request.getParameter("y");
        String z = request.getParameter("z");
        String fileName = y + ".png";
        return "forward:/mts/Hybrid/RoadTiles/"+z+"/"+x+"/"+fileName;
    }
}
